class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :name
      t.string :surname
      t.references :group
      t.date :birthdate
      t.string :email
      t.inet :registration_ip
      t.datetime :registered_at
      t.float :avg_rating

      t.timestamps
    end
  end
end
