class RegisterStudent

  def initialize(params, registration_ip)
    @params = params
    @registration_ip = registration_ip
  end

  def call
    student = Student.new(@params)
    student.registration_ip = @registration_ip
    student.registered_at = DateTime.current
    student
  end

end
