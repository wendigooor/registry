module StudentsHelper

  def format_russian_date(date)
    date.strftime('%d/%m/%Y')
  end

  def format_russian_datetime(datetime)
    datetime.strftime('%d/%m/%Y %I:%M')
  end

end
