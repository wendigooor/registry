class Student < ApplicationRecord
  belongs_to :group, optional: true

  validates  :name,
             :surname,
             :birthdate,
             :email,
             presence: true

end
